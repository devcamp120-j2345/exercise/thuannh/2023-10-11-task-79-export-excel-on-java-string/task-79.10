package com.devcamp.exportexcel.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.exportexcel.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
